#include<stdio.h>
int main() {
      double first, second, temp;
      printf("Enter number 1: ");
      scanf("%lf", &first);
      printf("Enter number 2: ");
      scanf("%lf", &second);

     
      temp = first;

      
      first = second;

      
      second = temp;

      printf("\nAfter swapping, number 1 = %.2lf\n", first);
      printf("After swapping, number 2 = %.2lf", second);
      return 0;
}