#include <stdio.h>

int main()
{
    int a;
      
    printf("Enter a number: ");
    scanf("%d", &a);
    

    if(a > 0)
    {
        printf("The number you entered is a POSITIVE number");
    }
    if(a < 0)
    {
        printf("The number you entered is a NEGATIVE number");
    }
    if(a == 0)
    {
        printf("The number you entered is ZERO");
    }

    return 0;
}