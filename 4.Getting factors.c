#include <stdio.h>
int main() {
    int a, b;
    printf("Enter any positive integer: ");
    scanf("%d", &a);
    printf("Factors of %d are: ", a);
    for (b = 1; b <= a; ++b) {
        if (a % b == 0) {
            printf("%d ", b);
        }
    }
    return 0;
}