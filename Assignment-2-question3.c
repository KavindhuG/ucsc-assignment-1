#include <stdio.h>

int main()
{
    char x;

    printf("Please enter any letter: \n");
    scanf(" %c", &x);
    
    if(x == 'a' || x == 'e' || x == 'i' || x == 'o' || x == 'u' ||
		x == 'A' || x == 'E' || x == 'I' || x == 'O' || x == 'U')  {
		printf("\n %c is a VOWEL.", x);
	}
    else {
    	printf("\n %c is a CONSONANT.", x);
	}
    return 0;
}