#include <stdio.h>
int main() {
    int a, b;
    printf("Enter any integer: ");
    scanf("%d", &a);
    for (b = 1; b <= a; ++b) {
        printf("%d * %d = %d \n", a, b, a * b);
    }
    return 0;
}